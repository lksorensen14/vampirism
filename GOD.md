### Executive summary (game concept, genre, target audience, project scope, etc.)

- Game Concept

A group of humans (10-12) have to fend off against a couple of vampires (1-2). 
If a human dies they turn into a vampire. If the humans kill off all the vampires they win.
If a vampire dies they die permanently. If the vampires kill off all of the humans they win.

- Genre

This is a cat and mouse game involving elements of rts and rpg.

- Target Audience

Anyone who might like elements of the above genre? (more info needed here)

- Project Scope

Initially one platform on PC for release.

---

### Gameplay (objectives, game progressions, in-game GUI, etc.)

- Objective

If you are a human your goal is to survive and kill all vampires.
If you are a vampire your goal is to kill all humans before they become to strong and eventually kill you.

- Game progression

Humans start with 100 wood and 0 gold.
Humans spawn initially and have 1 minute to run away from the middle of the map.
Humans have the ability to teleport short distances based on cooldown timer and recharge timer. 
Humans begin to gather resources by spawning workers out of houses.
Humans can increase their teams population cap by creating tents.
Once the player feels comfortable that they have enough resources they can pick a base location and start building.
Base locations are areas segmented across the map and are each designated by a choke point entrace (some locations may have two for team bases).
The choke point is 4 tiles wide. A player can protect a base location by building a wall on the 2 middle tiles of the chokepoint. This prevents the vampire from entering freely into the base.
Once this is made a human can then build towers behind the wall to attack the vampire when they are attacking the wall.
If a vampire attacks a base location the player can teleport the human to the wall and repair the wall while the towers damage the vampire.
There are upgrade progression trees for towers and walls.
To initially defend hotspots on the map a human can also create a hero tavern and spawn a hero providing some support.
Heroes can be leveled up every minute by standing in the pool of heroes located on the map.
After 15 minutes have passed each human receives 1 gold or more depending on how many humans and vampires are left on the map. This gold can be used to create higher tier infrastructure and workers.

Vampires start off with 0 wood and 200 gold.
Vampires spawn after 1 minute and can then search for the humans and kill them on the map.
Vampires cannot teleport.
Vampires can initially kill humans within two hits of the humans health pool.
Each time a vampire destroys a building or kills an entity it gains experience and can level up, unlocking additional abilites such as heal and far sight.
There is a chance that a vampire will drop a gold coin that a hero can pick up if the vampire does not pick it up in time.
Vampires also have access to different minion and item in the game through the use of markets that each open up after a certain amount of time.
Vampires periodically receive additional gold.
There are upgrade progression trees for items.
After a certain amount of time has elapsed vampires receive wood that can be traded for special abilites and stat upgrades. Eg. 2000 points to armour and strength.

There are also 4 portals located in regions on the map that allow humans and vampires to teleport long distances.

- In-game GUI

Human Screen
Vampire Screen

---

### Mechanics (rules, combat, physics, etc.)

- Rules

Humans and vampires cannot team together.

- Combat

TBA

- Physics

Towers when firing use an AOE or missile tracking attack type.
All objects that are upgradable or destuctable have a physics based animation that transforms the old object into the new one.

---

### Game elements (worldbuilding, story, characters, locations, level design, etc.)

- Worldbuilding
- Story
- Characters
- Locations
- Level Design

---

### Assets (music, sound effects, 2D/3D models, etc.)

- Mood Board (Style of Visualization)
- Colour Scheme
- Objects
- Music
- Sound Effects
- 2D/3D models

---

### Platform Release Plan

- Market release platform
- Anti-Cheat engine?
- Multiplayer Network Management
- Multiplayer Profile Management

---

### Technical Guidelines

#### Asset Management

** Naming Convention **

- [ ] All assets should be named in the following format:
<Parent><Object><Identifier><Number>.<FileType> (eg. Scene_Environment_Obstacle_1.png / Player_Control_Movement.cs)

** Version Convention **

- [ ] All Versioning should be named in the following format:
<Class><VersionNo><BuildNo><DateTime><Status> 
<Class> = Development, Alpha, Beta, NULL
<Status> = Production, Nightly, Development, Staging
(eg. Alpha 1.0.0 2201 26/04/2022 Production)

** Folder Structure **
[Parent]
    - Scripts
        - Player
        - Camera
        - Inventory
        - Environment
        - etc...
    - Assets
        - Prefabs
            - Items
            - Inventory
            - Characters
            - Environments
            - etc...
        - Materials
            - Characters
            - Environments
            - etc...
        - Models
            - Characters
            - Environments
            - etc...
        - Animations
        - Textures
            - Tiles
            - Palette
            - etc...
        - Effects
        - Shaders
        - RayTracing
        - HDRI
        - etc...
    - Packages
        - 2DSprite
        - TileMapEditor
        - CinemaMachine
        - PackageManager
        - etc...
    - Scenes
        - etc...
    - Tools
        - etc...

** Packages ** 

[Sam]
    - 



[Kaz]
    - CinemaMachine


