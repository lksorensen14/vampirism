
#### Prior Game Design Documentation referenced below (Use this to create the official GDD):
Game Design Document for Vampirism Incarnate

---

Acknowledgement of Ownership: 

--- 	

### Premise (Game Idea):

A group of humans (10-12) have to fend off against a couple of vampires (1-2). 
If a human dies they turn into a vampire. If the humans kill off all the vampires they win.
If a vampire dies they die permanently. If the vampires kill off all of the humans they win.

---

### Inspiration:

Vampirism Fire mod from Warcraft III.

---

### Story:
 
Null VOID... Ya Muppet.

---

### Characters:

---

### Level/environment design:

The environment needs to have measured units to work properly. Humans need to have the ability to go through one (unit) gap spaces where vampires can only go through two (unit) gap spaces. Essentially humans will build objects at choke points to stop the vampires from reaching them. 

The choke points can be 4 units wide. The humans build walls that are 2 units wide and are placed on either side. Humans can run through the edge gaps but vampires have to destroy the walls to advance.

** 2.5D or 3D (Undecided at this point). Need to discuss with Liam. **

“ I reckon we go for 2.5D with a rotating camera so essentially 3D. I mean really whatever would be suited for you. I have the graphics sides of things taken care of so yeah. 

I can Handle Server Hosting also, So if need be we can take care of that also. “ - Liam (Kaz)

---

### Gameplay:

**Humans Utilise the following:**

- Walls
- Towers
- Tents
- Mines
- Towns
- Workers
- Heroes

---

**Vampires utilize the following:**

- Shops
- University
- Portals
- Combo upgrades

---

### Resources:

Wood
Gold
Executive summary (game concept, genre, target audience, project scope, etc.)

---

### Art:	

---

### Sound and Music:

---

### User Interface, Game Controls: 

Human Screen
Vampire Screen

---

### Project Info 

- **[ ] Development: Yes**
- **[ ] Engine: Unreal Engine **
- **[ ] Repository System: Gitlab **
- **[ ] Development Platform: Unity**
- **[ ] Release Platform/s: Steam, Epic (Not decided yet) ** 
- **[ ] Project Management: Teams?**

---

### Production: 

- [ ] Alpha:
- [ ] Beta: 
- [ ] Release: 

---
